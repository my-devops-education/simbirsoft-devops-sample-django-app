# Party Parrot App
![parrot](media/images/party-parrot.gif)
  
Sample Python application on Django with PostgreSQL database.

### Requirements

* django 4.0.1
* Pillow 9.0.0
* psycopg2-binary 2.9.3
* django-prometheus 2.2.0

### Build

On Alpine install libs

```shell
apk add --no-cache jpeg-dev zlib-dev
apk add --no-cache --virtual .build-deps build-base linux-headers
```

```shell
pip install --no-cache-dir -r requirements.txt
```

```Dockerfile
FROM python:3.10-alpine3.18

WORKDIR /app
COPY requirements.txt .
RUN apk add --no-cache jpeg-dev zlib-dev \
    && apk add --no-cache --virtual .build-deps build-base linux-headers \
    && pip install --no-cache-dir -r requirements.txt

COPY . .
```

```yml
version: "3.8"

services:
  app:
    build:
      context: .
      dockerfile: ./.configs/Dockerfile
    command: >
      sh -c "python manage.py migrate &&
             python manage.py runserver 0.0.0.0:${DJANGO_PORT:-8080}"
    environment:
      DJANGO_DB_HOST: "db"
      DJANGO_DB_USER: "${POSTGRES_USER:-root}"
      DJANGO_DB_PASS: "${POSTGRES_PASSWORD:-password}"
      DJANGO_DB_NAME: "${POSTGRES_DB:-root}"
      DJANGO_DB_PORT: "${POSTGRES_PORT:-5432}"
      DJANGO_DEBUG: "${DJANGO_DEBUG:-False}"
    ports:
      - "${APP_PORT:-8080}:${DJANGO_PORT:-8080}"
    restart: unless-stopped
    healthcheck:
      test: "curl --fail http://localhost:${DJANGO_PORT:-8080}/health || exit 1"
      interval: 40s
      timeout: 30s
      retries: 3
      start_period: 60s
    volumes:
      - app_uploads:/app/media/uploads
    depends_on:
      - db
    networks:
      - back-tier
      - front-tier

  db:
    image: postgres:15.5-alpine3.18
    environment:
      POSTGRES_USER: "${POSTGRES_USER:-root}"
      POSTGRES_PASSWORD: "${POSTGRES_PASSWORD:-password}"
      POSTGRES_DB: "${POSTGRES_DB:-root}"
      PGPORT: "${POSTGRES_PORT:-5432}"
    restart: unless-stopped
    healthcheck:
      test: ["CMD-SHELL", "pg_isready", "-d", "${POSTGRES_DB:-root}"]
      interval: 40s
      timeout: 30s
      retries: 3
      start_period: 60s
    # ports:
    #   - "${POSTGRES_PORT:-5432}:${POSTGRES_PORT:-5432}"
    volumes:
      - pg_data:/var/lib/postgresql/data
    networks:
      - back-tier

  prometheus:
    image: prom/prometheus:v2.48.0
    restart: unless-stopped
    healthcheck:
      test: "curl --fail http://localhost:9090/metrics || exit 1"
      interval: 40s
      timeout: 30s
      retries: 3
      start_period: 60s
    volumes:
      - prometheus_data:/prometheus
      - ".configs/prometheus/prometheus.yml:/etc/prometheus/prometheus.yml"
    # ports:
    #   - 9090:9090
    networks:
      - back-tier

  postgres-exporter:
    image: quay.io/prometheuscommunity/postgres-exporter:v0.15.0
    restart: unless-stopped
    environment:
      DATA_SOURCE_NAME: "postgresql://${POSTGRES_USER:-root}:${POSTGRES_PASSWORD:-password}@db:${POSTGRES_PORT:-5432}/${POSTGRES_DB:-root}?sslmode=disable"
    healthcheck:
      test: "curl --fail http://localhost:9187/metrics || exit 1"
      interval: 40s
      timeout: 30s
      retries: 3
      start_period: 60s
    # ports:
    #   - 9187:9187
    depends_on:
      - db
    networks:
      - back-tier

  grafana:
    image: grafana/grafana:10.2.2-ubuntu
    environment:
      GF_SECURITY_ADMIN_USER: "${GF_SECURITY_ADMIN_USER:-admin}"
      GF_SECURITY_ADMIN_PASSWORD: "${GF_SECURITY_ADMIN_PASSWORD:-password}"
      GF_HTTP_PORT: "${GF_HTTP_PORT:-3000}"
    depends_on:
      - prometheus
    ports:
      - "${GF_PUBLIC_HTTP_PORT:-8081}:${GF_HTTP_PORT:-3000}"
    volumes:
      - grafana_data:/var/lib/grafana
      - .configs/grafana/provisioning/:/etc/grafana/provisioning/
    networks:
      - back-tier
      - front-tier
    restart: always

volumes:
  pg_data:
  app_uploads:
  prometheus_data:
  grafana_data:

networks:
  front-tier:
  back-tier:
```

### Deployment

#### Set environments

In file .env add environments  
Example file
```ini
APP_PORT=8080
POSTGRES_USER=root
POSTGRES_PASSWORD=Lk_end3MB-Whdm
POSTGRES_DB=root
POSTGRES_PORT=5432
DJANGO_PORT=8080
DJANGO_DEBUG=False
GF_SECURITY_ADMIN_USER=admin
GF_SECURITY_ADMIN_PASSWORD=K3nb-JDj_3knB!YH
GF_HTTP_PORT=3000
GF_PUBLIC_HTTP_PORT=8081
```

#### Environments descriptions

| Env | Default | Description|
|---|---|---|
| APP_PORT | 8080 | Set public application port |
| POSTGRES_USER | root | Set database user name |
| POSTGRES_PASSWORD | password | Set database user password |
| POSTGRES_DB | root | Set database name |
| POSTGRES_PORT | 5432 | Set database port |
| DJANGO_DEBUG | False | Enable django debug |
| DJANGO_PORT | 8080 | Set django internal port |
| GF_SECURITY_ADMIN_USER | admin | Set grafana username |
| GF_SECURITY_ADMIN_PASSWORD | password | Set grafana password |
| GF_HTTP_PORT | 3000 | Set grafana internal port |
| GF_PUBLIC_HTTP_PORT | 8081 | Set grafana public port |

### Monitoring

#### Prometheus
Config file .configs\prometheus\prometheus.yml
```yml
global:
  scrape_interval: 30s
scrape_configs:
  - job_name: prometheus
    static_configs:
      - targets:
        - prometheus:9090
  - job_name: grafana
    static_configs:
      - targets:
        - grafana:3000        
  - job_name: django
    static_configs:
      - targets:
        - app:8080
        labels:
          app: 'Parrot'      
  - job_name: 'postgres'
    static_configs:
      - targets:
        - db:5432
    metrics_path: /metrics
    relabel_configs:
      - source_labels: [__address__]
        target_label: __param_target
      - source_labels: [__param_target]
        target_label: instance
      - target_label: __address__
        replacement: postgres-exporter:9187
```

#### Grafana
Folder .configs\grafana\provisioning\dashboards contains dashboard for grafana
Folder .configs\grafana\provisioning\datasources\prometheus.yml contains prometheus datasources for grafana
```yml
apiVersion: 1

datasources:
  - name: Prometheus
    type: prometheus
    access: proxy
    orgId: 1
    url: http://prometheus:9090
    basicAuth: false
    isDefault: true
    editable: true
```
  
Dashboards
* Grafana
![Grafana](images/Dashboards/Grafana.png)
* Prometheus
![Prometheus](images/Dashboards/Prometheus.png)
* Postgres
![Postgres](images/Dashboards/Postgres.png)
* Django
![Django](images/Dashboards/Django.png)